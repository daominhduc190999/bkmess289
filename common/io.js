let socket_io = require('socket.io');
let io = socket_io();
let socketAPI = {};
let userList= [{
    id: 0,
    username: "BKBot",
}];

//Your socket logic here
socketAPI.io = io;

io.on('connection', async (socket)=>{
    console.log("connected! id: ", socket.id );
    // Event: new user registers and connects
    socket.on('signin', (username) => {
        userList.push({
            id: socket.id,
            username: username,
        })
        console.log(userList);
        // broadcast new user to everyone
        io.emit('new user',{
            id: socket.id,
            username: username
        })
        io.emit('updated list', userList)
        console.log(socket.id, username, "@");
    })
    // Event: client disconnects with server
    socket.on('disconnect', function(){
        for(let i = 0 ; i < userList.length; i++){
            if(userList[i].id == socket.id){
                io.emit('disconnect user', userList[i])
                userList.splice(i);
                break;
            }
        }
    })
    

    // Event: new user sends a message to server
    socket.on('message', function (data){
        let {id, message, senderID} = data;
        
        console.log("message from: ", senderID, " to: ", id, " content:", message,)
        
        // if not a message to bot:
        if(id != 0){
            for(let i = 0; i < userList.length; i ++){
                if(userList[i].id == id){
                    io.to(id).emit('coming message', {message, senderID});
                    break;
                }
            }
        }      
    })
})


io.on('disconnect', async (socket)=>{

})

module.exports= {
    socketAPI,
    userList
}
